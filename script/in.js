function getJsonRes() {
    const socket = new WebSocket("ws://10.10.1.179:8080/parking");

    var today = new Date();
    var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var dateTime = date+' '+time;

    const parkingId = "P" + Math.floor(Math.random() * 100);   
    const platNo = document.getElementById("platNo").value;
    const tipeKendaraan = document.getElementById("tipeKendaraan").value;

    const jsonReq = JSON.stringify({
        parking_id: parkingId,
        plat_no: platNo,
        tipe_kendaraan: tipeKendaraan,
        date_time: dateTime,
    });

    socket.onopen = function (event) {
        socket.send("Hello From Client!");
        socket.send(jsonReq);
    };

    socket.onmessage = function (event) {
        parkirMasuk(event.data, dateTime)
    };
}

function parkirMasuk(res, dateTime) {
    if (res !== "") {
        let result = "";
        const parkirInRes = document.getElementById("parkirIn");
        const jsonObj = JSON.parse(res);

        result += `<div class="result-in">Id Parkir: ${jsonObj["data"].id_parkir}</div>
        <div class="result-in">Plat Nomor: ${jsonObj["data"].jam_masuk}</div>
        <div class="result-in">Jam Masuk: ${dateTime}</div>` 

        parkirInRes.innerHTML = result;
        console.log(jsonObj);

    }
}

function parkirKeluar() {}
 